import {
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import {
  NutrientFact,
  Product,
  SupplierProduct,
  UnitOfMeasure,
  Recipe,
} from "./supporting-files/models";
import {
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
  ConvertUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

interface Nutrients {
  [k: string]: NutrientFact;
}

interface SupplierProductExtended extends SupplierProduct {
  costPerBaseUnit: number;
  productName: string;
  brandName: string;
}
const extendSupplierProduct = (
  product: Product,
  supplierProduct: SupplierProduct
): SupplierProductExtended => {
  return {
    ...supplierProduct,
    costPerBaseUnit: GetCostPerBaseUnit(supplierProduct),
    productName: product.productName,
    brandName: product.brandName,
  };
};

const findCheapestSupplierPerBaseUnit = (
  supplierList: SupplierProductExtended[]
) => {
  return supplierList.sort((a, b) => a.costPerBaseUnit - b.costPerBaseUnit)[0];
};

const findCheapestSupplierByPrice = (
  supplierList: SupplierProductExtended[]
) => {
  return supplierList.sort((a, b) => a.supplierPrice - b.supplierPrice)[0];
};

const filterSupplier = (
  supplierProducts: SupplierProductExtended[],
  ingredientsNeeded: UnitOfMeasure
) => {
  return supplierProducts.filter((x) => {
    let amountNeeded: number;

    // Workaround: Couldn't convert cups to kilogram
    try {
      amountNeeded = ConvertUnits(
        ingredientsNeeded,
        x.supplierProductUoM.uomName,
        x.supplierProductUoM.uomType
      ).uomAmount;
    } catch (err) {
      amountNeeded = ingredientsNeeded.uomAmount;
    }

    return x.supplierProductUoM.uomAmount <= amountNeeded;
  });
};

const main = (recipe: Recipe) => {
  let cheapestCost = 0;
  const nutrientsAtCheapestCost: Nutrients = {};

  recipe?.lineItems.forEach((lineItem, i) => {
    let bought: SupplierProductExtended[] = [];

    const products = GetProductsForIngredient(lineItem.ingredient);
    // console.log("products", products);

    let mergedSupplierProducts: SupplierProductExtended[] = [];
    products.forEach((product) => {
      const supplierProductsExtended = product.supplierProducts.map((x) =>
        extendSupplierProduct(product, x)
      );

      mergedSupplierProducts = [
        ...mergedSupplierProducts,
        ...supplierProductsExtended,
      ];
    });
    // console.log("mergedSupplierProducts", mergedSupplierProducts);

    const boughtWithoutFilter: SupplierProductExtended[] = [];
    const boughtWithFilter: SupplierProductExtended[] = [];

    let ingredientsNeeded = JSON.parse(JSON.stringify(lineItem.unitOfMeasure));
    while (ingredientsNeeded.uomAmount > 0) {
      const cheapestSupplier = findCheapestSupplierPerBaseUnit(
        mergedSupplierProducts
      );
      // console.log("cheapestSupplier", cheapestSupplier);

      try {
        ingredientsNeeded = ConvertUnits(
          ingredientsNeeded,
          cheapestSupplier.supplierProductUoM.uomName,
          cheapestSupplier.supplierProductUoM.uomType
        );
      } catch (err) {}

      ingredientsNeeded.uomAmount -=
        cheapestSupplier.supplierProductUoM.uomAmount;

      boughtWithoutFilter.push(cheapestSupplier);
    }

    // buying smaller size might be more efficient
    ingredientsNeeded = JSON.parse(JSON.stringify(lineItem.unitOfMeasure));
    while (ingredientsNeeded.uomAmount > 0) {
      const filteredSuppliers = filterSupplier(
        mergedSupplierProducts,
        ingredientsNeeded
      );

      let cheapestSupplier;
      if (filteredSuppliers.length) {
        cheapestSupplier = findCheapestSupplierPerBaseUnit(filteredSuppliers);
      } else {
        cheapestSupplier = findCheapestSupplierByPrice(mergedSupplierProducts);
      }

      try {
        ingredientsNeeded = ConvertUnits(
          ingredientsNeeded,
          cheapestSupplier.supplierProductUoM.uomName,
          cheapestSupplier.supplierProductUoM.uomType
        );
      } catch (err) {}

      ingredientsNeeded.uomAmount -=
        cheapestSupplier.supplierProductUoM.uomAmount;

      boughtWithFilter.push(cheapestSupplier);
    }

    const sumBoughtWithoutFilter = boughtWithoutFilter.reduce(
      (partialSum, x) => partialSum + x.supplierPrice,
      0
    );
    const sumBoughtWithFilter = boughtWithFilter.reduce(
      (partialSum, x) => partialSum + x.supplierPrice,
      0
    );

    if (sumBoughtWithFilter < sumBoughtWithoutFilter) {
      bought = [...bought, ...boughtWithFilter];
    } else {
      bought = [...bought, ...boughtWithoutFilter];
    }

    // console.log("bought", bought);

    ingredientsNeeded = JSON.parse(JSON.stringify(lineItem.unitOfMeasure));
    bought.forEach((ingredient) => {
      try {
        ingredientsNeeded = ConvertUnits(
          ingredientsNeeded,
          ingredient.supplierProductUoM.uomName,
          ingredient.supplierProductUoM.uomType
        );
      } catch (err) {}

      let uomAmount = 0;
      if (
        ingredientsNeeded.uomAmount > ingredient.supplierProductUoM.uomAmount
      ) {
        uomAmount = ingredient.supplierProductUoM.uomAmount;
        ingredientsNeeded.uomAmount -= ingredient.supplierProductUoM.uomAmount;
      } else {
        uomAmount = ingredientsNeeded.uomAmount;
      }
      const cost = uomAmount * ingredient.costPerBaseUnit;
      cheapestCost += cost;

      const product = products.find(
        (x) =>
          x.brandName === ingredient.brandName &&
          x.productName === ingredient.productName
      );
      product?.nutrientFacts.forEach((nutrient) => {
        const nutrientFact = GetNutrientFactInBaseUnits(nutrient);

        if (!nutrientsAtCheapestCost[nutrientFact.nutrientName]) {
          nutrientsAtCheapestCost[nutrientFact.nutrientName] = nutrientFact;
        } else {
          nutrientsAtCheapestCost[nutrientFact.nutrientName] = {
            ...nutrientsAtCheapestCost[nutrientFact.nutrientName],
            quantityAmount: {
              ...nutrientFact.quantityAmount,
              uomAmount:
                nutrientsAtCheapestCost[nutrientFact.nutrientName]
                  .quantityAmount.uomAmount +
                nutrientFact.quantityAmount.uomAmount,
            },
          };
        }
      });
    });
  });

  recipeSummary[recipe.recipeName] = {
    cheapestCost,
    nutrientsAtCheapestCost,
  };
};

recipeData.forEach((recipe) => {
  main(recipe);
});

console.log(
  "recipeSummary",
  recipeSummary,
  recipeSummary["Creme Brulee"].nutrientsAtCheapestCost
);
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
